/**
 *Author:Toby Huang z5141448
 *Time of Create:20 Mar 2017
 *Purpose:print a + shape of star
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int size;

int isStar(int x, int y);

int main(int argc, char const *argv[]) {
    /* x and y represent the coordinate, size mean the square size */
    int x,y;
    printf("Enter size: " );
    scanf("%d",&size );
    for ( y = 0; y < size; y++) {
        /* for all y at that coordinate */
        for ( x = 0; x < size; x++) {
            /* for all x at that coordinate */
            if (isStar(x,y)) {
                /* code */
                printf("*" );
            }
            else {
                printf("-" );
            }
        }
        printf("\n" );
    }
    return EXIT_SUCCESS;
}


int isStar(int x, int y){
    if  (x== size/2 || y== size /2 ) {
        /* code */
        return 1;
    }
    else {
        return 0;
    }
}
