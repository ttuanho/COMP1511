/**
 *Author:Toby Huang z5141448
 *Time of Create:21 Mar 2017
 *Purpose:print the number can be devided by 3 or five less than
 *the number is given
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    /* */
    int number, thisNumber ;
    printf("Enter number: " );
    scanf("%d",&number );
    for ( thisNumber = 1; thisNumber < number; ++thisNumber) {
        /* loop all the number */
        if (thisNumber %3 ==0 || thisNumber%5 ==0)
            printf("%d\n", thisNumber);
    }

    return EXIT_SUCCESS;
}
