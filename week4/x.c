/**
 *Author:Toby Huang z5141448
 *Time of Create:20 Mar 2017
 *Purpose:print a X shape of star
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char const *argv[]) {
    /* x and y represent the coordinate, size mean the square size */
    int x,y,size;
    printf("Enter size: " );
    scanf("%d",&size );
    for ( y = 0; y < size; y++) {
        /* for all y at that coordinate */
        for ( x = 0; x < size; x++) {
            /* for all x at that coordinate */
            if (x+y==size -1 || x== y) {
                /* code */
                printf("*" );
            }
            else {
                printf("-" );
            }
        }
        printf("\n" );
    }
    return EXIT_SUCCESS;
}
