/**
 *Author:Toby Huang z5141448
 *Time of Create:21 Mar 2017
 *Purpose:print prime factor of the given number, if it is print, print that out
 *the number is given
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char const *argv[]) {
    /* */
    int number,thisNum, remain;
    printf("Enter number: " );
    scanf("%d",&number );
    remain=number;
    for ( thisNum = 2; thisNum < number; ++thisNum) {
        /* for every possible number */
        while (remain % thisNum==0) {
            /* if thisNum is the factor of given number */
            if (remain!=number) {
                /* printf the * sign */
                printf(" * " );
            }
            else{
                printf("The prime factorization of %d is:\n",number );
            }
            printf("%d", thisNum );
            // refresh the remain
            remain =remain/thisNum;
        }
        if (remain == thisNum) {
            break;
        }
    }

    if (remain==number)
        // print if this number is a prime
        printf("%d is prime\n", number);
    else
        // print is this ths not a prime
        printf(" = %d\n",number );
    return EXIT_SUCCESS;
}
