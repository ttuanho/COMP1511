/**
 *Author:Toby Huang z5141448
 *Time of Create:20 Mar 2017
 *Purpose:print an snail number and count at right position
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int size, roundId;
// rondCol and roundRow is the Row and Col that this round
int roundRow, roundCol;

int isStar(int x, int y);
int isStarX(int x, int y);
int isStarY(int x, int y);
int inInterval(int x, int a, int b);
int abs(int num);
int countDistance (int x,int y);
int calSeqId(int x,int y);
int mainSequence(int n);
int printLastDigit(int n);
int printNum(int x, int y);
int main(int argc, char const *argv[]) {
    /* x and y represent the coordinate, size mean the square size */
    int x,y;
    printf("Enter size: " );
    scanf("%d",&size );
    for ( y = 0; y < size; y++) {
        /* for all y at that coordinate */
        for ( x = 0; x < size; x++) {
            /* for all x at that coordinate */
            if (isStar(x,y)) {
                /* code */
                // printf ("1");
                printNum(x,y);
            }
            else {
                printf("-" );
            }
        }
        printf("\n" );
    }
    return EXIT_SUCCESS;
}


int isStar(int x, int y){
    return isStarX(x,y)||isStarY(x,y);
}

int isStarX(int x, int y){
    int n;
    if (y%2==0) {
        /* code */

        n=y/2;
        if (y>(size/2)){

            x-=1;
        }
        else {
        }
        if (inInterval(x,-2+2*n,size-1-2*n)){
            return 1;
        }
    }
    return 0;
}
int isStarY(int x, int y){
    int n;
    if (x%2==0) {

        n=x/2;
        if (x>=(size/2)){
            if (inInterval(y,2*n,size-1-2*n)){
                return 1;
            }
        }
        else {
            if (inInterval(y,2+2*n,size-1-2*n)){
                return 1;
            }
        }
    }
    return 0;
}

int inInterval(int x, int a, int b){
    return (abs(x-a)+abs(x-b)==abs(a-b));

}
int abs(int num){
    //get the absolute value
    if (num<0){
        return -num;
    }
    else{
        return num;
    }
}
int countDistance (int x,int y){
    //get the countable distance form this coordinate till round point
    // printf(" the distance now is %d +%d , sum is %d\n", abs(x- roundCol ),abs(y - roundRow),abs(x- roundCol )+abs(y - roundRow));
    // printf(" the col(x) %d row(y) %d at (%d,%d ) \n",roundCol, roundRow, x,y );
    return abs(x- roundCol )+abs(y - roundRow);

}
int calSeqId(int x,int y){
    /* get the round Id to use to represent the sequence id */

    // calculate these two vars, this is temporary answer
    roundRow= (roundId/2)*2; // here is the problem was made
    // printf(" here round row is %d at (%d, %d ) \n", roundRow,x ,y);
    roundCol = size -1 - roundRow;
    if (roundId%2==1) {
        /* on the downside of snail swap the temp answer */
        int swap;
        swap = roundRow;//here is the problem
        roundRow =roundCol;
        roundCol =swap;
    }

    if (y == roundRow|| x== roundCol) {
        /* this coordinate is on this round */
        // change the roundCol to the coordinate of startPoint of sequence
        if (roundId%2==0){
            //while the round is on upside
            roundCol =size - roundCol -3;
        }
        else if (roundId%2==1){
            // while the round is on downside
            roundCol =size -roundCol -1;
        }
        return (size/2 +1)- roundId;
    } else {
        /* this coordinate is not on this round */
        roundId +=1;
        return calSeqId( x,  y );
    }
}
int mainSequence(int n){
    // return the count of sequence
    n=2*n-1;
    return n-1+(n*n -1)/2+2;
}
int printLastDigit(int n){
    printf("%d", n%10);
}
int printNum(int x, int y){
    roundId = 0;
    // calSeqId(x,y);
    //
    // printf("sequence Id is %d\n, roundId now is %d ", calSeqId (x,y),roundId );
    printLastDigit(mainSequence(calSeqId(x,y))-countDistance(x,y));
}
