/**
 *Author:Toby Huang z5141448
 *Time of Create:21 Mar 2017
 *Purpose:print the factor of a number, and judge whether it is a perfect num
 *the number is given
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    /* */
    int number, thisNumber,factorSum;
    factorSum=0;
    printf("Enter number: " );
    scanf("%d",&number );
    printf("The factors of %d are:\n",number );
    for ( thisNumber = 1; thisNumber < number; ++thisNumber) {
        /* loop all the number */
        if (number % thisNumber ==0){
            //sum up the factor of given number
            printf("%d\n", thisNumber);
            factorSum+=thisNumber;
        }
    }
    printf("%d\n", number );
    printf("Sum of factors = %d\n%d", factorSum+number,number);
    if (factorSum==number) {
        /* whether it is a perfect number */
        printf(" is a perfect number\n");
    }else{
        printf(" is not a perfect number\n" );
    }
    return EXIT_SUCCESS;
}
