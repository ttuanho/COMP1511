/**
 *Author:Toby Huang z5141448
 *Time of Create:20 Mar 2017
 *Purpose:print a snall of star
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int size;

int isStar(int x, int y);
int isStarX(int x, int y);
int isStarY(int x, int y);
int inInterval(int x, int a, int b);
int abs(int num);

int main(int argc, char const *argv[]) {
    /* x and y represent the coordinate, size mean the square size */
    int x,y;
    printf("Enter size: " );
    scanf("%d",&size );
    for ( y = 0; y < size; y++) {
        /* for all y at that coordinate */
        for ( x = 0; x < size; x++) {
            /* for all x at that coordinate */
            if (isStar(x,y)) {
                /* code */
                printf("*" );
            }
            else {
                printf("-" );
            }
        }
        printf("\n" );
    }
    return EXIT_SUCCESS;
}


int isStar(int x, int y){
    return isStarX(x,y)||isStarY(x,y);
}
int isStarX(int x, int y){
    int n;
    if (y%2==0) {
        /* code */
        n=y/2;
        if (y>(size/2)){

            x-=1;
        }
        else {

        }
        if (inInterval(x,-2+2*n,size-1-2*n)){
            return 1;
        }
    }


    return 0;
}
int isStarY(int x, int y){
    int n;
    if (x%2==0) {

        n=x/2;
        if (x>=(size/2)){
            if (inInterval(y,2*n,size-1-2*n)){
                return 1;
            }
        }

        else {
            if (inInterval(y,2+2*n,size-1-2*n)){
                return 1;
            }
        }

    }
    return 0;
}

int inInterval(int x, int a, int b){
    return (abs(x-a)+abs(x-b)==abs(a-b));

}
int abs(int num){
    //get the absolute value
    if (num<0){
        return -num;
    }
return num;
}
