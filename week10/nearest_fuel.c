/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 9 May 2017
 *Purpose: Functions to found the distance to nearest fueal station.
**/
#include <stdio.h>
#include "trader_bot.h"
int nearest_fuel(struct bot *b){
    /* found in two way */
    int next_len=0;
    int pre_len=0;
    for (struct location *i = b->location; i->type != LOCATION_PETROL_STATION; i=i->next) {
        /* seek Petrol station by next */
        next_len ++;
    }
    for (struct location *i = b->location; i->type != LOCATION_PETROL_STATION; i=i->previous) {
        /* seek Petrol station by previous */
        pre_len ++;
    }
    /* return the lagrer num */
    if (next_len<= pre_len) return next_len; else return -pre_len;
}
