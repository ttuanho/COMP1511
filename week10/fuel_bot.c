/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 9 May 2017
 *Purpose: Bot only doing fueling action.
**/
#include <stdio.h>
#include "trader_bot.h"

int get_sign(int num){
    if (num>=0){
        return 1;
    }
    else {
    return -1;
    }
}
void get_action(struct bot *bot, int *action, int *n){
    if (bot->fuel==bot->fuel_tank_capacity &&
        bot->location->type == LOCATION_PETROL_STATION ) {
        /* move maximum length at it's full of fuel */
        *action = ACTION_MOVE;
        *n = bot->maximum_move;
    }
    else if(bot->location->type != LOCATION_PETROL_STATION ){
        /* seek and move to the fuel station */
        *action = ACTION_MOVE;
        int fuel_station_distance= nearest_fuel(bot);
        if (fuel_station_distance >= bot->maximum_move) {
            /* bot can not reach the fuel in one turn */
            *n = get_sign(fuel_station_distance)*bot->maximum_move;
        }
        else{
            *n = fuel_station_distance;
        }
    }
    else if (bot->location->type == LOCATION_PETROL_STATION){
        /* buy some fuel at fuel station */
        *action = ACTION_BUY;
        *n = bot->fuel_tank_capacity - bot->fuel;
    }
}
