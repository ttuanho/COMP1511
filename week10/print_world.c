/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 9 May 2017
 *Purpose: Functions of print the robot world.
**/
#include <stdio.h>
#include "trader_bot.h"


void print_location(struct location *this_location) {
    /* print the location by its type */
    if (this_location->type == LOCATION_START) {
        /* print the start type */
        printf("%s: start\n",this_location->name );
    }
    else if(this_location->type==LOCATION_SELLER){
        printf("%s: will sell %d units of %s for $%d\n",this_location->name,this_location->quantity, this_location->commodity->name, this_location->price);
    }
    else if(this_location->type==LOCATION_BUYER){
        printf("%s: will buy %d units of %s for $%d\n",this_location->name,this_location->quantity, this_location->commodity->name, this_location->price);
    }
    else if(this_location->type==LOCATION_PETROL_STATION){
        printf("%s: Petrol station %d units of available fuel for $%d\n",this_location->name, this_location->quantity, this_location->price );
    }
    else if(this_location->type==LOCATION_DUMP){
        printf("%s: dump\n",this_location->name );
    }
    else if(this_location->type==LOCATION_OTHER){
        printf("%s: other\n",this_location->name );
    }
}
void print_world(struct bot *b){
    struct location *start_location;
    start_location = b->location;
    // like do while, but print the first location, so the loop could work correctly
    print_location(start_location);
    // loop till the this_location is start_location
    for( struct location *this_location=start_location->next; this_location != start_location; this_location = this_location->next){
        print_location(this_location);
    }


}
