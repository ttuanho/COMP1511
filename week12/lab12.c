/*
 *Author: Toby HUANG z5141448 Luca CHEN z5155740
 *Date of Create: 15 May 2017
 *Purpose: Functions manipualte the linklist.
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct node {
    struct node *next;
    int       data;
};

struct node *create_node(int data, struct node *next);
struct node *last(struct node *head);
struct node *append(struct node *head, int value);
struct node *delete_first(struct node *head);
struct node *delete_last(struct node *head);
struct node *delete_contains(int i, struct node *head);
void print_list(struct node *head);
struct node *reverse(struct node *list);

#define MAX_LINE 4096

// simple main function to test delete_first, delete_last, delete_contains, reverse

int
main(int argc, char *argv[]) {
    char line[MAX_LINE];
    struct node *list_head = NULL;

    while (1) {
        printf("list = ");
        print_list(list_head);
        printf("\n");

        printf("> ");
        if (fgets(line, MAX_LINE, stdin) == NULL) {
            return 0;
        }

        int i = 0;
        while (isalpha(line[i]) || line[i] == '_') {
            i++;
        }
        int argument = atoi(&line[i]);
        line[i] = '\0';

        if (strcmp(line, "quit") == 0) {
            return 0;
        } else if (strcmp(line, "append") == 0) {
            list_head = append(list_head, argument);
        } else if (strcmp(line, "delete_first") == 0) {
            list_head = delete_first(list_head);
        } else if (strcmp(line, "delete_last") == 0) {
            list_head = delete_last(list_head);
        } else if (strcmp(line, "delete_contains") == 0) {
            list_head = delete_contains(argument, list_head);
        } else if (strcmp(line, "reverse") == 0) {
            list_head = reverse(list_head);
        } else if (strcmp(line, "") != 0) {
            printf("Unknown command: '%s'\n", line);
        }
    }
}

// delete first node in list

struct node *delete_first(struct node *head) {
    // REPLACE LINE THIS WITH YOUR CODE
    if (head != NULL) {
        /* Prevent list underflow */
        struct node *first_node = head;
        // refresh new head pointer before delete the first_node
        head = head->next;
        free(first_node);
    }
    return head;
}

// delete last node in list

struct node *delete_last(struct node *head) {
    // REPLACE LINE THIS WITH YOUR CODE
    if (head != NULL&& head->next !=NULL) {
        /* the link list at least have 2 node */
        struct node *this = head, *previous;
        while (this->next != NULL) {
            previous = this;
            this = this->next;
        }
        // make the last node disappear in list
        previous->next = NULL;
        // make the last node disappear in memory
        free(this);
    }
    else if (head!= NULL && head->next == NULL) {
        /* this list only have one node, so the first node is the last node */
        head = delete_first(head);
    }
    return head;
}

// delete first node containing specified int

struct node *delete_contains(int i, struct node *head) {
    // REPLACE LINE THIS WITH YOUR CODE
    struct node *previous;
    for (struct node *this = head; this != NULL; this= this->next) {
        /* read throght all the item in the list */
        if (this->data == i) {
            /* have found the require data, delete this node */
            if (this== head) {
                /* this is the first node */
                return delete_first(head);
            }
            else{
                /* delete the node */
                previous->next = this->next;
                free(this);
                return head;
            }

        }
        // set the previous
        previous= this;
    }
    // if haven't found anything, return the old list
    return head;
}

// reverse the nodes in list

struct node *reverse(struct node *head) {
    // REPLACE LINE THIS WITH YOUR CODE
    if (head == NULL) {
        /* do nothing while it's given a empty linklist */
        return head;
    }


    struct node *this = head,*previous = NULL,*next = this->next;
    while ( next != NULL) {
        /* read throught all the node */

        // refresh next
        next= this->next;
        // reorginaze the linklist
        this->next = previous;



        // refresh this and previous
        previous = this;
        if (next!=NULL) {
            /* refresh the this, if next is NULL, prepare to end the loop */
            this = next;
        }
    }
    return this;
}

// print contents of list in Python syntax

void print_list(struct node *head) {
    printf("[");
    for (struct node *n = head; n != NULL; n = n->next) {
        printf("%d", n->data);
        if (n->next != NULL) {
            printf(", ");
        }
    }
    printf("]");
}

// return pointer to last node in list
// NULL is returned if list is empty

struct node *last(struct node *head) {
    if (head == NULL) {
        return NULL;
    }

    struct node *n = head;
    while (n->next != NULL) {
        n = n->next;
    }
    return n;
}

// create a new list node containing value
// and append it to end of list

struct node *append(struct node *head, int value) {
    // new node will be last in list, so next field is NULL
    struct node *n =  create_node(value, NULL);
    if (head == NULL) {
        // new node is now  head of the list
        return n;
    } else {
        // change next field of last list node
        // from NULL to new node
        last(head)->next = n;  /* append node to list */
        return head;
    }
}

// Create a new struct node containing the specified data,
// and next fields, return a pointer to the new struct node.

struct node *create_node(int data, struct node *next) {
    struct node *n;

    n = malloc(sizeof (struct node));
    if (n == NULL) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    n->data = data;
    n->next = next;
    return n;
}
