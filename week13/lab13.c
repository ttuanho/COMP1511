#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct node {
    struct node *next;
    int       data;
};

int count(int value, struct node *list);
struct node *get_nth(int n, struct node *head);
struct node *insert_nth(int n, struct node *new_node, struct node *head);
struct node *delete_nth(int n, struct node *head);
struct node *delete_odd(struct node *head);

void print_list(struct node *head);
struct node *reverse(struct node *list);
struct node *create_node(int data, struct node *next);
struct node *last(struct node *head);
struct node *append(struct node *head, int value);
void free_list(struct node *head);

#define MAX_LINE 4096

// simple main function to test count, get_nth, insert_nth, delete_nth, delete_odd

int
main(int argc, char *argv[]) {
    char line[MAX_LINE];
    struct node *list_head = NULL;

    while (1) {
        printf("list = ");
        print_list(list_head);
        printf("\n");

        printf("> ");
        if (fgets(line, MAX_LINE, stdin) == NULL) {
            free_list(list_head);
            return 0;
        }

        int i = 0;
        while (isalpha(line[i]) || line[i] == '_') {
            i++;
        }
        int argument = atoi(&line[i]);
        line[i] = '\0';

        if (strcmp(line, "append") == 0) {
            list_head = append(list_head, argument);
        } else if (strcmp(line, "count") == 0) {
            printf("count(%d) returns %d\n", argument, count(argument, list_head));
        } else if (strcmp(line, "get_nth") == 0) {
            struct node *n = get_nth(argument, list_head);
            if (n == NULL) {
                printf("get_nth(%d) returned NULL\n", argument);
            } else {
                printf("get_nth(%d) returned a pointer to a node containing %d\n", argument, n->data);
            }
        } else if (strcmp(line, "delete_nth") == 0) {
            list_head = delete_nth(argument, list_head);
        } else if (strcmp(line, "insert_nth") == 0) {
            // insert a node containing 42
            struct node *new_node = create_node(42, NULL);
            list_head = insert_nth(argument, new_node, list_head);
        } else if (strcmp(line, "delete_odd") == 0) {
            list_head = delete_odd(list_head);
        } else if (strcmp(line, "") != 0) {
            printf("Unknown command: '%s'\n", line);
        }
    }
}


// handy code for delete the node that passed in
// private function

struct node *delete_this(struct node *head){
    if (head != NULL) {
        /* have this node that could be delete */
        // the node that would return
        struct node *return_node = head->next;
        // free the frirst node
        free(head);
        return return_node;
    }
    else{
        // couldn't delete, return what passed in
        return head;
    }
}


// Return number of nodes in the list containing value.

int count(int value, struct node *head) {
    int count = 0;
    for (struct node *i = head; i != NULL; i= i ->next) {
        /* read throught all the node in the node */
        if (i->data == value) {
            /* found the required value */
            count ++;
        }
    }
    return count;
}

// Return a pointer to the node in  position n in the list.
// Position 0 is the first node in the list.
// Return NULL if the list has no n-th node.

struct node *get_nth(int n, struct node *head) {

    int count = 0;
    for (struct node *i = head; i != NULL; i= i->next) {
        /* read throught all the node in the list */
        if (count == n) {
            /* have count that required node */
            // so this is the node is asked for
            return i;
        }
        // auto increase, counting the node have read
        count ++;
    }

    // don't have that n_th item in the list, so return nothing
    return NULL;
}

// Delete the node in  position  n in the list.
// The first node in the list is in position 0.
// Do nothing if there is no position n in the list.
// The new head of the list is returned.

struct node *delete_nth(int n, struct node *head) {

    // handy var to record the previous item
    struct node *previous = NULL;
    // record the node should be delete
    struct node *delete_node= NULL;

    // count the nodes that has read
    int count = 0;
    for (struct node *i = head; i != NULL; i= i->next) {
        /* read throught all the node */
        if (count == n) {
            /* have found the required node */
            delete_node = i;
            // end of this loop
            break;
        }
        // set the previous node to memory.
        previous = i;
        // auto increase the count
        count ++;
    }

    if (delete_node == NULL) {
        /* break this function, because couldn't foun't the node should be delete */
    }
    else{
        /* have something to delete */
        if (previous == NULL) {
            /* first node is the node should be delete */
            head = delete_this(delete_node);
        }
        else{
            // else situation
            previous->next = delete_this(delete_node);
        }

    }
    // return this new list
    return head;
}

// Insert new_node before position n in the list.
// The first node in the list is in position 0.
// If n == length of the list, new_node is appended to list.
// Otherwise do nothing if there is no position n in the list.
// The new head of the list is returned.

struct node *insert_nth(int n, struct node *new_node, struct node *head) {


    if (n-1 == -1) {
        /* insert at the first node */
        new_node->next = head;
        head = new_node;
    }
    else {

        // count the nodes that has read
        int count = 0;
        for (struct node *i = head; i != NULL; i= i->next) {
            /* read throught all the node */
            if (count == n-1) {
                /* have found the required node's previous node */
                // rearrange the list
                new_node->next = i->next;
                i->next = new_node;
                // end this loop
                break;
            }
            // increase count
            count ++;
        }
    }
    // return the new head
    return head;
}

// Delete all nodes of the list containing odd numbers.
// The new head of the list is returned.

struct node *delete_odd(struct node *head) {
    // handy var to record the previous item
    struct node *previous = NULL;
    // record the node should be delete
    struct node *delete_node= NULL;

    // count the nodes that has read
    int count = 0;
    for (struct node *i = head; i != NULL; i= i->next) {
        /* read throught all the node, while delete the odd */
        if (i->data%2 == 1) {
            /* have found the required node, which have odd data in it */

            // set the node should be delete
            delete_node = i;
            // delete this node
            if (previous == NULL) {
                /* first node is the node should be delete */
                while (head != NULL && head->data%2 == 1){
                    // delete all the node that is odd till one is not odd
                    // then get the index of that to i
                    head = delete_this(head);
                }
                if(head == NULL){
                    // have deleted all the node in the list, end this loop
                    break;
                }
                i = head;
            }
            else{
                // turn the index back to the node that could be delete
                i= previous;
                // else situation
                previous->next = delete_this(delete_node);
            }

        }
        // set the previouss
        previous = i;
    }
    return head;
}


// print contents of list in Python syntax

void print_list(struct node *head) {
    printf("[");
    for (struct node *n = head; n != NULL; n = n->next) {
        printf("%d", n->data);
        if (n->next != NULL) {
            printf(", ");
        }
    }
    printf("]");
}

// return pointer to last node in list
// NULL is returned if list is empty

struct node *last(struct node *head) {
    if (head == NULL || head->next == NULL) {
        return head;
    }
    return last(head->next);
}

// create a new list node containing value
// and append it to end of list

struct node *append(struct node *head, int value) {
    // new node will be last in list, so next field is NULL
    struct node *n =  create_node(value, NULL);
    if (head == NULL) {
        // new node is now  head of the list
        return n;
    } else {
        // change next field of last list node
        // from NULL to new node
        last(head)->next = n;  /* append node to list */
        return head;
    }
}

// Create a new struct node containing the specified data,
// and next fields, return a pointer to the new struct node.

struct node *create_node(int data, struct node *next) {
    struct node *n;

    n = malloc(sizeof (struct node));
    if (n == NULL) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    n->data = data;
    n->next = next;
    return n;
}

void free_list(struct node *head) {
    if (head != NULL) {
        free_list(head->next);
        free(head);
    }
}
