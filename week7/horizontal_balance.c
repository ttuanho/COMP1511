/*
 *Author: Toby Huang
 *Date of Create: 14 April
 *Purpose: Print out the Calculated value of horizontal_balance.
 */
 #include <stdio.h>
 #include "captcha.h"

double get_horizontal_balance(int height, int width, int pixels[height][width]){
    int column_sum=0,n_black_pixels=0;
    for (int x = 0; x < height; x++) {
        /* for each row */
        for (int y = 0; y < width; y++) {
            /* for each column */
            if (pixels[x][y]==1) {
                /* here is a black pixel */
                column_sum += y;
                n_black_pixels++;
            }
        }
    }
    return ((double)column_sum/n_black_pixels + 0.5)/width;
}
