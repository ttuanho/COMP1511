/*
 *Author: Toby Huang
 *Date of Create: 11 April
 *Purpose: Copy pixel by pixel to the array copy which have the number picture
 *         without the broader.
 */
 #include <stdio.h>
 #include "captcha.h"

 void copy_pixels(int height, int width, int pixels[height][width],
                 int start_row, int start_column, int copy_height, int copy_width,
                 int copy[copy_height][copy_width])
{
    /* for each pixels that needs to be copy into new array*/
    for (int x = 0; x < copy_height; x++) {
        /* for each row */
        for (int y = 0; y < copy_width; y++) {
            /* for each column */
            // x+start_row is the index of original array
            // so does y + start_column
            copy[x][y]=pixels[x + start_row][y + start_column];
        }
    }
}
