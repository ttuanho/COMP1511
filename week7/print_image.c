/*
 *Author: Toby Huang
 *Date of Create: 11 April
 *Purpose: Print the pbm image to stdout in the right style.
 */
#include <stdio.h>
#include "captcha.h"


void print_image(int height, int width, int pixels[height][width]){
    for (int x = height-1; x >= 0; x--) {
        /*
        select the elements of pixels in height,
        because it was inversed by height, so it will start from the biggest
        number of height to print from the top to buttom
        */
        for (int y = 0; y < width; y++) {
            /* select the pixel in the width */
            // print the pixel out by the style of .=0 *=1
            if (pixels[x][y] == 0) {
                /* if this pixel is 0, print . */
                printf("." );

            } else {
                /* if this pixel is 1, print * */
                printf("*");
            }
        }
        // print the next line at the end of each line of pixels
        printf("\n");
    }
}
