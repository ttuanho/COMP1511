/*
 *Author: Toby Huang
 *Date of Create: 11 April
 *Purpose: Calculate the boundary box inforamtion, using pointer to transfer
 *         varibles.
**/
#include <stdio.h>
#include "captcha.h"

void get_bounding_box(int height, int width, int pixels[height][width],
                      int *start_row, int *start_column, int *box_height,
                      int *box_width)
{
    // initial the vars that would return
    *start_row = height;
    *start_column = width;
    // initial intermidia vars that could form the box_width and box_height
    int end_row = 0,end_column = 0;
    for (int x = 0; x < height; x++) {
        /* for each pixel in the column */
        for (int y = 0; y < width; y++) {
            /* for each pixel in the row */
            if (pixels[x][y] == 1 ) {
                /* if this pixels have something, could refresh the value of
                   *start_row,*start_column,end_row,end_column
                */
                if (*start_row > x ) {
                    /* *start_row should be the smallest row num that pixels
                    exist */
                    *start_row = x;
                }
                if (*start_column > y) {
                    /* *start_column should be the smallest column num that
                       pixels exist */
                    *start_column = y;
                }
                if (end_row < x) {
                    /* end_row should be biggest row num that pixels exist */
                    end_row = x;
                }
                if (end_column < y) {
                    /* end_column should be biggest column num that pixels
                       exist */
                    end_column = y;
                }
            }
        }
    }
    /* Calculate the value of box_width and box_height to return */
    *box_width = end_column- *start_column + 1;
    *box_height = end_row- *start_row + 1;



}
