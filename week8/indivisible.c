#include <stdio.h>
int id[1000],id_size=0;
int input[1000],input_size=0;
int print_array(int array_size,int array[array_size]){
    for (int i=0; i< array_size; i++){
        printf(" %d",array[i]);
    }
    printf("\n");
}

int check_divide(int i){
    for (int j=0; j< input_size; j++){
        // Check whether it could be divide.
        if (i!=j&&input[i]%input[j]==0){
            // it could be divide by some number in the input
            return 1;
        }
    }
    return 0;
}

int main(int argc, char argv[]){
    

    while (scanf("%d", &input[input_size])!=EOF){
        input_size++;
    }
    for (int i=0; i< input_size; i++){
        // for each item in input array, then check its' divisiable
        if(check_divide(i)==0){
            id[id_size]=input[i];
            id_size++;
        }

    }
    

    printf("Indivisible numbers were:");
    print_array(id_size, id);

}
