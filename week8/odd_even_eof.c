#include <stdio.h>

int print_array(int array_size,int array[array_size]){
    for (int i=0; i< array_size; i++){
        printf(" %d",array[i]);
    }
    printf("\n");
}

int main(int argc, char argv[]){
    int this_num;
    int odd[1000],odd_size=0;
    int even[1000],even_size=0;
    while (scanf("%d", &this_num)!=EOF){
        if(this_num%2==0){
            // this is an even number
            even[even_size]=this_num;
            even_size++;
        }
        else{
            odd[odd_size]= this_num;
            odd_size++;
        }
    }
    printf("Odd numbers were:");
    print_array(odd_size, odd);
    printf("Even numbers were:");
    print_array(even_size, even);
}
