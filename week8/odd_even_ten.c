#include <stdio.h>

int print_array(int array_size,int array[array_size]){
    for (int i=0; i< array_size; i++){
        printf(" %d",array[i]);
    }
    printf("\n");
}

int main(int argc, char argv[]){
    int ten[10];
    int odd[10],odd_size=0;
    int even[10],even_size=0;
    for (int i=0; i < 10; i++){
        scanf("%d", &ten[i]);
        if(ten[i]%2==0){
            // this is an even number
            even[even_size]=ten[i];
            even_size++;
        }
        else{
            odd[odd_size]= ten[i];
            odd_size++;
        }
    }
    printf("Odd numbers were:");
    print_array(odd_size, odd);
    printf("Even numbers were:");
    print_array(even_size, even);
}


