#include <stdio.h>

int main(int argc, char argv[]){
    int ten[10];
    for (int i=0; i < 10; i++){
        scanf("%d", &ten[i]);
    }
    printf("Numbers were:");
    for (int i=0; i < 10; i++){
        printf(" %d", ten[i]);
    }
    printf("\n");
    return 0;
}
