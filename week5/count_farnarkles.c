/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: Count farnakle of two array, which is same number at same position.
 *Language:C
 */
#include <stdio.h>
#include "farnarkle.h"

// return number of farnarkles
int count_farnarkles(int tiles[N_TILES], int guessTiles[N_TILES]) {
    // franarkles is for storing the number of franarkles
    // i is for loop
    int franarkles=0,i=0;
    while (i < N_TILES) {
        /* for each iteam in both array */
        if (tiles[i]==guessTiles[i]) {
            /* at correct position then add one to franarkles */
            franarkles++;
        }
        i++;
    }
    return franarkles; // return the number of franarkles
}
