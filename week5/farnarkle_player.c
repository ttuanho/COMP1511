/**
 *Author:Toby Huang z5141448 Adam Stucci z5157372
 *Time of Create:31 Mar 2017
 *Purpose: Guess an tiles by looking up the possible array that remain in a
 *big array.
 *Language:C
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "farnarkle.h"

// an automated farnarkle_player
// given all previous guesses and their farnarkles and arkle counts
// make a new guess
//
// inputs:
// turn - which turn this is
// previous_guesses contains the turn - 1 previous_guesses
// farnarkles[i] contains the number of farnarkles for previous_guess[i]
// arkles[i] contains the number of arkles for previous_guess[i]
//
// output:
// guess - the next guess

int isThisPossible(int thisSequence[N_TILES], int thisGuess[N_TILES],int thisFarnarkle,int thisArkle);
long long int getIntialMaxPossible();

void farnarkle_player(int turn, int previous_guesses[MAX_TURNS][N_TILES], int farnarkles[MAX_TURNS], int arkles[MAX_TURNS], int guess[N_TILES]) {
    int possibleNum=0,thisDigit=0;
    // calculate the max number of possible of this setting
    long long int maxPossible=getIntialMaxPossible();
    // printf("max possible is %lld\n",maxPossible );
    // to store how many possible have found
    int thisPossible=0;
    int possible[maxPossible][N_TILES];
    // i,j,k for the loop
    int i=0,j=0,k=0,remainDigit=0;
    while (i<maxPossible) {
        /* initial the array of possible */
        // get the position of this number
        remainDigit=i;
        j=N_TILES;
        while (j>0) {
            /* each item in the loop, this algrithom is from the last digit */
            thisDigit=remainDigit%MAX_TILE+1;
            possible[i][j]=thisDigit;
            remainDigit/=MAX_TILE;
            j--;
        }
        i++;
    }


    /* for every previous_guesses in the array */
    i=1;
    while (i<turn) {
        /* select the previous_guesses */
        j=0;
        // reset the var of thisPossible
        thisPossible=0;
        while (j<maxPossible) {
            /* for every possiblity */
            if (isThisPossible(possible[j],previous_guesses[i],farnarkles[i],arkles[i])) {
                /* the possible Sequence might be the hidden Sequence */
                k =0;
                while (k<N_TILES) {
                    /* refresh the possible Sequence */
                    possible[thisPossible][k]=possible[j][k];
                    k++;
                }
                thisPossible++;
            }
            // printf("%d\n",j );
            j++;
        }
        // refresh the maxPossible value
        maxPossible=thisPossible;
        i++;
    }


    /* find a possible guessTiles*/
    if (maxPossible<=1) {
        /* the last possbile is the answer */
        i=0;
        while (i<N_TILES) {
            /* input the possible into the guess array */
            guess[i]=possible[0][i];
            i++;
        }
    }
    else{
        // find an guess in the possible;
        srand(time(NULL));
        i=0;
        j=rand()%maxPossible;

        while (i<N_TILES) {
            /* transfer the possible array into guesss */
            guess[i]=possible[j][i];
            i++;
        }
    }

}
int isThisPossible(int thisSequence[N_TILES], int thisGuess[N_TILES],int thisFarnarkle,int thisArkle){
    int farnarkle,arkle;
    farnarkle=arkle=0;
    // i is for loop
    int franarkle=0,i=0;
    if (count_farnarkles(thisSequence,thisGuess)==thisFarnarkle && thisArkle==count_arkles(thisSequence,thisGuess)){
        return 1;
    }
    else {
        return 0;
    }

}

long long int getIntialMaxPossible(){
    /* get the number of MAX_TILE ^ N_TILES */
    int i=0;
    long long int answer=1;
    while (i<N_TILES) {
        // MAX_TILE time for n times
        answer*=MAX_TILE;
        i++;
    }
    return answer;
}
