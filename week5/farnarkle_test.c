/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose:Test the wrote functions with just one turn.
 *Language:C
 */
#include <stdio.h>
#include "farnarkle.h"

int main(void) {
    int hidden_sequence[N_TILES];
    int guess[N_TILES];
    int farnarkles;
    create_random_tiles(hidden_sequence);
    // put your code here

    while (farnarkles !=N_TILES) {
        /* guessing tiles */

        // read the guess tile
        printf("Enter guess: ");
        if (read_tiles(guess) != 1) {
            printf("Could not read guess\n");
            return 1;
        }

        farnarkles=count_farnarkles(hidden_sequence, guess);
        printf("%d farnarkles\n", farnarkles);
        printf("%d arkles\n", count_arkles(hidden_sequence,guess) );
    }
    printf("You win\n" );
    return 0;
}
