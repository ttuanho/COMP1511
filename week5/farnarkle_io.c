/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: Read the tiles and put it in the specific array, throw an error
 *message to terminal in non-successful situation.
 *Language:C
 */
#include <stdio.h>
#include "farnarkle.h"

// read N_TILES tiles into array tiles
// return 1 if successful, 0 otherwise
int read_tiles(int tiles[N_TILES]) {
    //thisNum is scaned number, countNum is the pointer in the array
    int thisNum,countNum;
    //initial the countNum
    countNum=0;
    while (countNum<N_TILES){
        if (scanf("%d",&thisNum )!=EOF &&thisNum<=MAX_TILE && thisNum >0) {
            /* successful input */
            tiles[countNum]=thisNum;
            countNum+=1;
        }
        else {
            /* uncorrect the input */
            return 0;
        }
    }
    // successfully input the MAX_TILE num
    return 1;
}

// print tiles on a single line
void print_tiles(int tiles[N_TILES]) {
    // print all items in the array
    int i = 0;
    while (i<N_TILES) {
        /* for all items in the array */
        printf("%d ", tiles[i] );
        i ++;
    }
    printf("\n" );
}
