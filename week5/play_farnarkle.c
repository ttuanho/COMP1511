/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: GAMES! GAMES! Compile this file correctly will obtain an playable
 *game.
 *Language:C
 */
#include <stdio.h>
#include "farnarkle.h"

int main(void) {
    int hidden_sequence[N_TILES];
    int guess[N_TILES];
    int farnarkles=0,turn =1;
    create_random_tiles(hidden_sequence);
    // put your code here

    while (farnarkles !=N_TILES) {
        /* guessing tiles */
        // read the guess tile
        printf("Enter guess for turn %d: ",turn);
        if (read_tiles(guess) != 1) {
            printf("Could not read guess\n");
            return 1;
        }

        farnarkles=count_farnarkles(hidden_sequence, guess);
        printf("%d farnarkles ", farnarkles);
        printf("%d arkles\n", count_arkles(hidden_sequence,guess) );
        turn ++;
    }
    printf("You win\n" );
    return 0;
}
