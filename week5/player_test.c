/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: Test file for the stupid AI that created. Can get the situation of
 *how the AI is behave, and have some result.
 *Language:C
 */
#include <stdio.h>

#include "farnarkle.h"

int main(void) {
    // hidden_sequence is to store the computer setted sequence
    int hidden_sequence[N_TILES];
    // these array are to store this guess and guess history
    int  previous_guesses[MAX_TURNS][N_TILES];
    int guess[N_TILES];
    // this array is to store the history of farnarkles and arkles
    int farnarkles[MAX_TURNS], arkles[MAX_TURNS];
    // turn is for the times the robot tried
    int turn=1;
    // i is for the loop
    int i=0;
    create_random_tiles(hidden_sequence);
    // put your code here
    printf("Hidden guess is:" );
    while (i<N_TILES) {
        /* print the hidden tiles */
        printf(" %d",hidden_sequence[i] );
        i++;
    }
    //prin a new line for the sequence
    printf("\n" );

    // if the computer doesn't success in last turn and turn number
    // dosen't hit MAX_TURNS, countinue the guess
    while (farnarkles[turn-1] !=N_TILES&&turn<MAX_TURNS) {
        /* guessing tiles */



        // let the computer guess
        farnarkle_player( turn, previous_guesses,  farnarkles,arkles, guess);

        // print out the computer guess
        printf("Player guess for turn %d:",turn);
        i=0;
        while (i<N_TILES) {
            /* for each item in guess array */
            printf(" %d",guess[i] );
            i++;
        }
        printf("\n" );


        // farnarkles and arkles to the array to record
        farnarkles[turn]=count_farnarkles(hidden_sequence, guess);
        arkles[turn]=count_arkles(hidden_sequence,guess);
        // print out this situation to monitor
        printf("%d farnarkles ", farnarkles[turn]);
        printf("%d arkles\n",arkles[turn] );


        // store this guess to previous_guesses array
        i=0;
        while (i<N_TILES) {
            /* for each item in this guess */
            previous_guesses[turn][i]=guess[i];
            i++;
        }



        // add one to the turn point
        turn ++;
    }
    //print the times that robot tried
    printf("Player took %d turns to guess code.\n",turn );
    return 0;
}
