/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: .h file for the all used functions and values of some static vars.
 *Language:C
 */
#define N_TILES 4
#define MAX_TILE 8

int read_tiles(int tiles[N_TILES]);
void print_tiles(int tiles[N_TILES]);

int count_farnarkles(int tiles[N_TILES], int guessTiles[N_TILES]);

int count_arkles(int tiles[N_TILES], int guessTiles[N_TILES]);


void create_random_tiles(int tiles[N_TILES]);
#define MAX_TURNS 100

void farnarkle_player(int turn, int previous_guesses[MAX_TURNS][N_TILES], int farnarkles[MAX_TURNS], int arkles[MAX_TURNS], int guess[MAX_TILE]);
