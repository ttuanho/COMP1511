/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: Test the functions in the farnakle_io, with just few lines.
 *Language:C
 */
#include <stdio.h>
#include "farnarkle.h"

int main(void) {
    int tiles[N_TILES];

    printf("Enter sequence: ");
    if (read_tiles(tiles) == 1) {
        printf("Sequence read was: ");
        print_tiles(tiles);
    } else {
        printf("Could not read tiles\n");
    }
}
