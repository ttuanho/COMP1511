/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: Create an random_tiles that is used in the hidden_sequence.
 *Language:C
 */
#include <stdlib.h>
#include <time.h>
#include "farnarkle.h"

// set tiles to pseudo-random values
void create_random_tiles(int tiles[N_TILES]) {
    int i;

    // seed (initialize) pseudo-random number generate with current time in seconds
    srand(time(NULL));

    i = 0;
    while (i < N_TILES) {
        // rand() returns a peudo-random integer in ranger 0 to RAND_MAX inclusive
        // convert to an integer in the range 1..MAX_TILE_template
        tiles[i] = rand() % MAX_TILE + 1;
        i = i + 1;
    }
}
