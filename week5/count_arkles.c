/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:26 Mar 2017
 *Purpose: Count the arkles of the hidden tiles by apply the rules given by the
 *question.
 *Language:C
 */
#include <stdio.h>
#include "farnarkle.h"

// return number of arkles
int count_arkles(int tiles[N_TILES], int guessTiles[N_TILES]){
    // arkles to store the arkles num; i,j for the loops
    int arkles=0,i=0;
    int tilesNums[MAX_TILE]={0};
    while (i<N_TILES) {
        /* record the times of num in tiles shown up */
        tilesNums[tiles[i]-1]++;
        i++;
    }
    //initial i
    i=0;
    while (i<N_TILES) {
        /* for each item in guessTiles */
        if (tilesNums[guessTiles[i]-1]>0) {
            /* if that have an arkle add one to the value */
            tilesNums[guessTiles[i]-1] --;
            arkles ++;
        }
        i++;
    }
    // subtract the number of farnarkles
    arkles-= count_farnarkles(tiles,guessTiles);
    return arkles; // replace with your code
}
