/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 1 May 2017
 *Purpose: Print a sequence of number in a specific file.
**/
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    /* read the input in command line */
    if (argc != 4) {
        /* doesn't have correct input info */
        printf("Usage %s lower_int higher_int file_name \n",argv[0] );
    }
    // vars that specify the feature of the number sequence
    int min =0, max = 0;
    // var of file_name
    char file_name[4096];
    // get two int from command line input
    min= atoi(argv[1]);
    max= atoi(argv[2]);
    if (!( min <= max)) {
        /* didn't input int or size of int not correct */
        printf("please input int\n" );
        return 1;
    }
    /* open the file and write */
    FILE *f;
    f=fopen(argv[3],"w");
    for (int i = min; i <= max; i++) {
        /* print the number sequence into file */
        fprintf(f, "%d\n",i );
    }

    return 0;
}
