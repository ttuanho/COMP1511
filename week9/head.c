/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 1 May 2017
 *Purpose: Print the head of a file.
**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[]) {
    /* read the input in command line */
    /* open the file and read */
    FILE *f;
    int max_lines =10;

    if (argc == 2) {
        /* no flag of n, read the file */
        f=fopen(argv[1],"r");
    }
    else if (argc ==4 && strcmp(argv[1],"-n")== 0) {
        /* has a flag of n   */
        // refresh the max_lines
        max_lines= atoi(argv[2]);
        // read the file at new argv[]
        f=fopen(argv[3],"r");
    }
    else {
        exit(1);
    }

    char this_char;
    int count_lines = 0;
    /* read all the file */

    while ((this_char=getc(f))!=EOF) {
        // print this char into screen
        putchar(this_char);
        if (this_char == '\n') {
            /* add one to count_lines */
            count_lines++;
            if (count_lines ==max_lines) {
                /* until this have read 10 or n lines */
                return 0;
            }
        }


    }

    return 0;
}
