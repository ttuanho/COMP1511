/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 1 May 2017
 *Purpose: Out put the input string within a colum.
**/

#include <stdio.h>

int main(int argc, char const *argv[]) {
    /* input and output the string */
    // promote word
    printf("Enter a string: " );

    // var to tmp record the char input
    char this_char;
    while ((this_char=getchar())!='\n') {
        /* assign the string i to this_char and output it */
        printf("%c\n",this_char );
    }


    return 0;
}
