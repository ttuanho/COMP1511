/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 4 May 2017
 *Purpose: Print the last 10 lines to the command line.
**/

#include<stdio.h>
#include<stdlib.h>


int main(int argc, char *argv[]){
    if (argc!=2){
        //print the error message
        fprintf(stderr,"Usage: %s file_location\n",argv[0]);
        return 1;
    }
    
    //start the programme
    FILE *f;
    if(!(f=fopen(argv[1], "r"))){
        printf("couldn't open the file\n");
        exit(1);
    }
    //store the start pointer of each line
    char this_line[4096];
    int lines=0;
    while(fgets(this_line,4096,f)){
        lines++;
    }
    
    rewind(f);
    int start_lines= lines - 10;
    lines = 0;
    while(fgets(this_line,4096,f)){
        // beacuse puts always will print a new line after end of string,
        // so use the fputs.
        lines++;
        if(lines> start_lines){
            fputs(this_line,stdout);
        }
    }
    
    return 0;
}
