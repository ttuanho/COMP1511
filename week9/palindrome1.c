/*
 *Author: Toby Huang z5141448 Adam Stucci z5157372
 *Date of Create: 2 May 2017
 *Purpose: Check wheter the input string (ignore space, and its case)
 *         is a same forwards as backwords.
**/

#include <stdio.h>
#include <ctype.h>
int is_palindrome(int string_size, char string[4096]);
int main(int argc, char const *argv[]) {
    /* input and output the string */
    // promote word
    printf("Enter a string: " );

    // var to tmp record the char input
    char string[4096];
    int i =0;
    while ((string[i]=getchar())!='\n') {
        /* read the string and input in the string array */
        i++;
    }
    if (is_palindrome(i,string)) {
        /* it is a palindrome */
        printf("String is a palindrome\n" );
     }
    else{
        /* it is not a palindrome */
        printf("String is not a palindrome\n" );
    }

    return 0;
}

int is_palindrome(int string_size, char string[4096]){
    int new_string_size=0;
    for (int i = 0; i < string_size; i++) {
        /* delete the space( and punct) and change the case for each char */
        if (string[i]!= ' ' && !ispunct(string[i])) {
            /* if not a space, store the lowwer case in the string array,
               so it would be ignore the space.*/
            string[new_string_size]=tolower(string[i]);
            // printf("%c\n",string[i] ); //debug
            new_string_size++;
        }
    }
    // refresh the string_size
    string_size=new_string_size;



    for (int i = 0; i < string_size; i++) {
        /* check whether is same while readding forwards and backwords */
        if (string[i]==string[string_size-1-i]) {
            /* check it char by char */
            if (i>string_size-1-i) {
                /* the index in two way have come across, it's palindrome */
                return 1;
            }
        }
        else{
            /* have string doesn't match the rules */
            return 0;
        }
    }


}
