/*
 *Author: Toby Huang
 *Date of Create: 1 May 2017
 *Purpose: Print the head of a file.
**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct string_ar {
    int strlen[20];
    char str[20][4096];
};
struct string_ar string_array;

int max_lines =10;
int refresh_string_array();
int main(int argc, char const *argv[]) {
    /* read the input in command line */
    /* open the file and read */
    FILE *f;

    if (argc == 2) {
        /* no flag of n, read the file */
        f=fopen(argv[1],"r");
    }
    else if (argc ==4 && strcmp(argv[1],"-n")== 0) {
        /* has a flag of n   */
        // refresh the max_lines
        max_lines= atoi(argv[2]);
        // read the file at new argv[]
        f=fopen(argv[3],"r");
    }
    else {
        exit(1);
    }

    // because it would push up an element, so
    max_lines++;

    char this_char;
    int count_lines = 0;
    /* read all the file, store it in to a string*/
    while ((this_char=getc(f))!=EOF) {
        // put this line of content into the array
        string_array.str[count_lines][string_array.strlen[count_lines]]=this_char;
        string_array.strlen[count_lines]++;
        if (this_char == '\n') {
            /* add one to count_lines */
            count_lines++;
            if (count_lines =max_lines) {
                /* already have 10 collection in array, rearrange the array */
                refresh_string_array();
                // refresh the count_lines to 10 (index of array is 9)
                count_lines = max_lines-1;
            }
        }

    }

    // printf("max_lines %d\n", count_lines);
    for (int i = 0; i < count_lines; i++) {
        /* print all the content in the array that stored */
        printf("%s",string_array.str[i] );
    }

    return 0;
}

int refresh_string_array(){
    struct string_ar new_array;
    for (int i = 1; i < max_lines; i++) {
        /* push up a unit, store it in new array*/
        strcpy(new_array.str[i-1],string_array.str[i]);

        new_array.strlen[i-1]=string_array.strlen[i];
    }
    //make a new one
    string_array=new_array;
}
