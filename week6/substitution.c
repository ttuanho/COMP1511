/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:2 Apr 2017
 *Purpose: Use substitution way to Encrypt a text.
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
// record the order of alphabet
char downcase[27]= "abcdefghijklmnopqrstuvwxyz";
char uppercase[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
char encryptDowncase[27]={'0'};
char encryptUppercase[27]={'0'};

int setKey();

int main(int argc, char const *argv[]) {
    /* Encrypt with certain way */
    // temp var to store the input char
    char ch;
    setKey(argv);
    while ((ch=getchar())!=EOF) {
        /* find the char loaction and put the encrypted char */
        for (int i = 0; i < 26; ++i) {
            /* store the location in to array */
            if (uppercase[i]==ch ) {
                /* print the uppercase encryped char */
                putchar(encryptUppercase[i]);
            }
            if ( downcase[i]==ch) {
                /* print the downcase encryped char */
                putchar(encryptDowncase[i]);
            }
        }
        if (ispunct(ch)||ch==' '||ch=='\n') {
            /* print the punctuation */
            putchar(ch);
        }
    }
    return 0;
}
int setKey(char const *argv[]){
    /* Get the key that put in the terminal */
    for (int i = 0; i < 26; i++) {
        /* get the key and put in in the arrays */
        encryptDowncase[i]=argv[1][i];
        for (int j = 0; j < 26; j++) {
            /* generate the encryptUppercase */
            if (downcase[j]==argv[1][i]) {
                /* get that chr into the encryptUppercase */
                encryptUppercase[i]=uppercase[j];
            }
        }
    }
}
