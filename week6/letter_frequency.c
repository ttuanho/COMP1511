/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:2 Apr 2017
 *Purpose: Record and print the letter frequency of a text.
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    /* Encrypt with certain way */
    // record the order of alphabet
    char downcase[27]= "abcdefghijklmnopqrstuvwxyz";
    char uppercase[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // temp var to store the input char
    char ch;
    // store the letter frequency
    int frequency[26]={0};
    // record the total sum of chars
    int sum=0;
    while ((ch=getchar())!=EOF) {
        /* find the char in both array  */
        for (int i = 0; i < 26; ++i) {
            /* store the location in to array */
            if (uppercase[i]==ch || downcase[i]==ch) {
                /* record in to the frequency */
                frequency[i]++;

            }
        }
        sum++;
    }
    /* print out the statistic */
    // store the percentage
    double percentage;
    for (int i = 0; i < 26; i++) {
        /* print the statistic by char */
        percentage=(double)frequency[i]/sum;
        printf("\'%c\' %f %d\n",downcase[i],percentage,frequency[i] );
    }
    return 0;
}
