/**
 *Author:Toby Huang z5141448
 *Time of Create:4 Apr 2017
 *Purpose: lab Questions
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char const *argv[]) {
    /* scan a string and print it without vowel */
    char ch;
    // alphabet set
    char downcase[27]= "abcdefghijklmnopqrstuvwxyz";
    char uppercase[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int countWords,punc,wordLength,maxWordLength,minWordLength,lastIsPunc;
    lastIsPunc=punc=wordLength=maxWordLength=minWordLength=0;
    countWords=1;
    while ((ch=getchar())!= EOF) {
        /* getting a char   */
        if (ispunct(ch)||ch==' '||ch=='\n') {
            /* print the char */
            if (wordLength>maxWordLength) {
                /* find new maxWordLength */
                maxWordLength=wordLength;
            }
            if (minWordLength==0||wordLength<minWordLength) {
                /* fin a new minWordLength */
                minWordLength=wordLength;
            }
            lastIsPunc=1;
            punc++;
        }
        else{
            if (lastIsPunc) {
                /* last chat is punctuation, set it to 0 */
                lastIsPunc=0;
                //refresh the record of wordLength
                wordLength=0;
                countWords++;
            }
            for (int i = 0; i < 26; i++) {
                /* find the char in alphabet */
                if (ch==downcase[i]||ch==uppercase[i]) {
                    /* find a character in the alphabet */
                    wordLength++;
                }
            }
        }
    }
    printf("Input contains %d blanks, tabs and new lines\n",punc);
    printf("Number of words: %d\n",countWords);
    printf("Length of shortest word: %d\n",minWordLength);
    printf("Length of longest word: %d\n", maxWordLength);

    return 0;
}
