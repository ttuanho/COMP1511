/**
 *Author:Toby Huang z5141448
 *Time of Create:4 Apr 2017
 *Purpose: lab Questions
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>
int getCharPosition(int charCount);
int getCenterSpaces(int height,int thisLayer);
int main(int argc, char const *argv[]) {
    /* scan a string and print it without vowel */
    int height,width=1,charCount=0;
    printf("Enter height: " );
    scanf("%d",&height );
    char uppercase[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (int i = 0; i < height; i++) {
        /* for a whole height of triangle */
        for (int j = 0; j < getCenterSpaces(height,i); j++) {
            /* print the space in each layer */
            printf(" ");
        }
        for (int j = 0; j < width; j++) {
            /* for the width of each layer */
            putchar(uppercase[getCharPosition(charCount)]);
            charCount++;
        }
        width +=2;
        printf("\n" );

    }
    return 0;
}
int getCharPosition(int charCount) {
    /* return this count in the alphabet position */
    return charCount%26;
}
int getCenterSpaces(int height,int thisLayer){
    return height-thisLayer-1;
}
