/**
 *Author:Toby Huang z5141448
 *Time of Create:4 Apr 2017
 *Purpose: lab Questions
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    /* scan a string and print it without vowel */
    char ch;
    int sum=0,digits=0;
    while ((ch=getchar())!= EOF) {
        /* getting a char   */
        if (ch=='1'||ch=='2'||ch=='3'||ch=='4'||ch=='5'||ch=='6'||ch=='7'||ch=='8'||ch=='9'||ch=='0') {
            /* print the char */
            sum+=atoi(&ch);
            digits++;

        }
    }
    printf("Input contained %d digits which summed to %d\n",digits,sum );
    return 0;
}
