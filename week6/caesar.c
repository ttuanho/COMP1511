/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:2 Apr 2017
 *Purpose: Encrypt by use the caeser.
 *Language:C
 */
#include <stdio.h>
#include <stdlib.h>

int getCharLocation(int initialLocation);
int casterAdd=0;
int main(int argc, char const *argv[]) {
    /* Encrypt with certain way */
    if (argc==2) {
        /* have an number input */
        casterAdd=atoi(argv[1]);
        while (casterAdd<0) {
            /* if the changed is less than 0, then changed it to an positive
               num */
            casterAdd+=26;
        }
    }
    else{
        // unexpected situation
        casterAdd=0;
    }
    // record the order of alphabet
    char downcase[27]= "abcdefghijklmnopqrstuvwxyz";
    char uppercase[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // temp var to store the input char
    char ch;
    while ((ch=getchar())!=EOF) {
        /* find the char in both array and then change it */
        for (int i = 0; i < 26; ++i) {
            /* check the loaction */
            if (uppercase[i]==ch) {
                /* found it in uppercase */
                ch=uppercase[getCharLocation(i)];
                break;
            }
            if (downcase[i]==ch) {
                /* found it in downcase */
                ch=downcase[getCharLocation(i)];
                break;
            }
        }
        // print out the Encrypt char
        putchar(ch);
    }


    return 0;
}
int getCharLocation(int initialLocation){
    // return the location of the Encrypt char
    return (initialLocation+casterAdd)%26;
}
