/**
 *Author:Toby Huang z5141448, Adam Stucci z5157372
 *Time of Create:2 Apr 2017
 *Purpose: Interpreate with string by print it without vowel.
 *Language:C
 */
#include <stdio.h>

int main(int argc, char const *argv[]) {
    /* scan a string and print it without vowel */
    char ch;
    while ((ch=getchar())!= EOF) {
        /* getting a char   */
        if (ch!='a' && ch !='e' && ch!='i'&& ch!='o' && ch != 'u'
            &&ch!='A'&& ch !='E' && ch!='I'&& ch!='O' && ch != 'U' ) {
            /* print the char */
            putchar(ch);

        }
    }

    return 0;
}
