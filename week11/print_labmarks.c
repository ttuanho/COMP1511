/*
 *Author: Toby HUANG z5141448 Luca CHEN z5155740
 *Date of Create: 15 May 2017
 *Purpose: Print the grade of lab, by read the source file specify in command
 *         line.
**/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MAX_STUDENT_NAME_LENGTH 128
#define MAX_GRADE_STRING_LENGTH 22
#define MAX_LAB_NAME_LENGTH 32
#define MAX_LINE_LENGTH 4096

struct student {
    int              zid;
    char             name[MAX_STUDENT_NAME_LENGTH + 1];
    char             lab_name[MAX_LAB_NAME_LENGTH + 1];
    char             lab_grades[MAX_GRADE_STRING_LENGTH + 1];
    struct student   *next;
};

struct student *read_students_file(char filename[]);
struct student *read_student(FILE *stream);

double grades2labmark(char grades[]);
int main(int argc, char *argv[]) {
    // CHANGE THIS CODE

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <marks-file> <lab_name>\n", argv[0]);
        return 1;
    }

    struct student *student_list = read_students_file(argv[1]);
    // tmp pointer to store the next student's pointer
    struct student *next;
    for (struct student *this_stu = student_list; this_stu != NULL;
         this_stu=next) {
        /* print selected students */
        if (!strcmp(argv[2],this_stu->lab_name)) {
            /* print this selected students */
            printf("%d %-30s %-12s %-22s %4.1lf\n",this_stu->zid,this_stu->name,
            this_stu->lab_name,this_stu->lab_grades,
            grades2labmark(this_stu->lab_grades));
        }
        // tmp pointer to store the next student's pointer
        next= this_stu->next;
        // free the struct have read
        free(this_stu);
    }
    return 0;
}

double grades2labmark(char grades[]){
    double grade = 0;
    for (int i = 0; grades[i]!='\0'; i++) {
        /* read till the array end */
        if (grades[i]!= '+') {
            if (grades[i]=='A') {
                /* get an A */
                grade += 1;
            }
            else if (grades[i] =='B') {
                grade +=0.8;
            }
            else if (grades[i] == 'C') {
                /* get a C TAT */
                grade += 0.5;
            }
            /*
            '.' is not attemp, but it act like have one class more, but gain
            no point at here
            */
        }
        else {
            /* only A have A+, act like A,B,C have class, + is add on */
            grade+= 0.2;
        }
    }
    /* Base on the total mark and the classes to calculate the grade */
    if (grade > 10) {
        /* getting too much grade here */
        grade = 10;
    }
    return grade;
}


// DO NOT CHANGE THE CODE BELOW HERE - DO NOT CHANGE read_students_file

// read_students_file reads a file where line contains information for 1 student
// it creates a linked of student structs containing the information
// it returns a pointer to the head of the list

struct student *read_students_file(char filename[]) {
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        fprintf(stderr,"warning file %s could not  be opened for reading\n"
                , filename);
        return NULL;
    }

    struct student *first_student = NULL;
    struct student *last_student = NULL;
    struct student *s;
    while ((s = read_student(fp)) != NULL) {
        if (last_student == NULL) {
            first_student = s;
            last_student = s;
        } else {
            last_student->next = s;
            last_student = s;
        }
    }
    return first_student;
}

// DO NOT CHANGE read_student

// read_student mallocs a student struct
// and reads a line in this format:
//
// 5099703 Tsun Bordignon thu13-sitar A+A+CABAB..A.
//
// stores the values in the struct field
// and returns a pointer to the struct

struct student *read_student(FILE *stream) {
    char line[MAX_LINE_LENGTH];

    struct student *s = malloc(sizeof (struct student));
    assert(s);

    if (fgets(line, MAX_LINE_LENGTH, stream) == NULL) {
        free(s);
        return NULL;
    }

    char *newline_ptr = strchr(line, '\n');
    assert(newline_ptr);
    *newline_ptr = '\0';

    char *space_ptr = strrchr(line, ' ');
    assert(space_ptr);
    strncpy(s->lab_grades, space_ptr + 1, MAX_GRADE_STRING_LENGTH);
    s->lab_grades[MAX_GRADE_STRING_LENGTH] = '\0';
    *space_ptr = '\0';

    space_ptr = strrchr(line, ' ');
    assert(space_ptr);
    strncpy(s->lab_name, space_ptr + 1, MAX_LAB_NAME_LENGTH);
    s->lab_name[MAX_LAB_NAME_LENGTH] = '\0';
    *space_ptr = '\0';

    space_ptr = strchr(line, ' ');
    assert(space_ptr);
    strncpy(s->name, space_ptr + 1, MAX_STUDENT_NAME_LENGTH);
    s->name[MAX_STUDENT_NAME_LENGTH] = '\0';
    *space_ptr = '\0';

    s->zid = atoi(line);
    s->next = NULL;
    return s;
}
