/*
 *Author: Toby HUANG z5141448 Luca CHEN z5155740
 *Date of Create: 15 May 2017
 *Purpose: Get the grade from command line and print the grade.
**/
#include <stdio.h>
#include <stdlib.h>
double grades2labmark(char grades[]);
int main(int argc, char *argv[]) {
    /* get the grade from command line and print the grade */
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <grade-string>\n", argv[0]);
        return 1;
    }


    printf("%.1lf\n",grades2labmark(argv[1]) );
    return 0;
}
double grades2labmark(char grades[]){
    double grade = 0;
    for (int i = 0; grades[i]!='\0'; i++) {
        /* read till the array end */
        if (grades[i]!= '+') {
            if (grades[i]=='A') {
                /* get an A */
                grade += 1;
            }
            else if (grades[i] =='B') {
                grade +=0.8;
            }
            else if (grades[i] == 'C') {
                /* get a C TAT */
                grade += 0.5;
            }
            /*
            '.' is not attemp, but it act like have one class more, but gain
            no point at here
            */
        }
        else {
            /* only A have A+, act like A,B,C have class, + is add on */
            grade+= 0.2;
        }
    }
    /* Base on the total mark and the classes to calculate the grade */
    if (grade > 10) {
        /* getting too much grade here */
        grade = 10;
    }
    return grade;

}
